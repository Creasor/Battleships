package app.battleships.logic;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import app.battleships.Constants;

public class GameBoardCell implements Parcelable, Serializable {
    /**
     * Static class used to define the contents
     * of a single GameBoard cell.
     */
    // Default is no hit (false).
    private boolean mShot;

    // Default to no ship at this cell.
    private Ship.Type mShipType = null;

    // Back pointer to this cells location on the board.
    private int nRow = -1;
    private int nCol = -1;

    public GameBoardCell(int row, int col) {
        setPosition(row, col);
    }

    /**
     * Sets the current row and column for this cell.
     *
     * @param row
     * @param col
     */
    private void setPosition(int row, int col) {
        assertValidPosition(row, col);
        nRow = row;
        nCol = col;
    }

    private void assertValidPosition(int row, int col) {
        if (!(0 <= row && row < Constants.GAME_BOARD_SIZE) ||
            !(0 <= col && col < Constants.GAME_BOARD_SIZE)) {
            throw new IllegalArgumentException("Invalid cell row or column");
        }
    }

    /**
     * Returns current row for this cell.
     *
     * @return
     */
    public int getRow() {
        return nRow;
    }

    /**
     * Returns current column for this cell.
     *
     * @return
     */
    public int getCol() {
        return nCol;
    }

    /**
     * Determine if this cell has shot at.
     *
     * @return true if this cell has been shot at.
     */
    private boolean isShot() {
        return mShot;
    }

    /**
     * Determine if this cell has a portion of a ship
     * that has been hit.
     *
     * @return true if this cell contains part of a ship
     * and has been hit by an opponent's shot.
     */
    public boolean isHit() {
        return mShot && mShipType != null;
    }

    /**
     * Determine if this cell has been shot at but
     * does not contain a ship.
     *
     * @return true if this cell does not contains
     * any part of a ship but has been shot at by
     * the opponent.
     */
    public boolean isMiss() {
        return mShot && mShipType == null;
    }

    /**
     * @return true if cell contains part of a ship.
     */
    public boolean containsShip() {
        return mShipType != null;
    }

    /**
     * @return type of ship that spans this cell.
     */
    public Ship.Type getShipType() {
        return mShipType;
    }

    /**
     * Sets this cell to contain part of the specified ship type.
     *
     * @param type
     */
    public void setShipType(Ship.Type type) {
        this.mShipType = type;
    }

    /**
     * Records this cell as having received a "shot". Caller is
     * responsible for only calling this method once per cell.
     */
    public void setShot() {
        if (isShot()) {
            throw new IllegalStateException(
                    "setShot should not only be called once per cell");
        }

        mShot = true;
    }

    @Override
    public String toString() {
        return "GameBoardCell{" +
                "mShot=" + mShot +
                ", mShipType=" + mShipType +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(mShot ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mShipType == null ? -1 : this.mShipType
                .ordinal());
    }

    private GameBoardCell(Parcel in) {
        this.mShot = in.readByte() != 0;
        int tmpShipType = in.readInt();
        this.mShipType = tmpShipType == -1 ?
                null : Ship.Type.values()[tmpShipType];
    }

    public static final Creator<GameBoardCell> CREATOR = new
            Creator<GameBoardCell>() {
                @Override
                public GameBoardCell createFromParcel(Parcel source) {
                    return new GameBoardCell(source);
                }

                @Override
                public GameBoardCell[] newArray(int size) {
                    return new GameBoardCell[size];
                }
            };

    /**
     * Clears this cell.
     */
    public void clear() {
        mShot = false;
        mShipType = null;
    }
}
