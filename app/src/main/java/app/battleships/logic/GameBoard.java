package app.battleships.logic;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Contains the implementation of a battleships game board
 * which can be initialized to an arbitrary size. The board
 * must be setup with exactly 1 ship for each ShipType and
 * these ships must be placed either strictly horizontally
 * or strictly vertically on the game board.
 */
public class GameBoard implements Serializable, Parcelable {
    private ArrayList<Ship> mShips;
    private final GameBoardCell[][] mGameBoard;
    private ArrayList<Integer> mGameTurns;
    private int mHits;
    private int mShots;
    private String mPlayer;

    /**
     * Constructor creates a GameBoard of size X size
     * GameBoardCells initialized to empty values.
     *
     * @param size
     */
    public GameBoard(int size) {
        mGameBoard = new GameBoardCell[size][size];
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                mGameBoard[row][col] = new GameBoardCell(row, col);
            }
        }

        // Create a list (set) of ships that are required
        // on any properly setup board. The current rules
        // require one instance of each ship type.
        mShips = new ArrayList<>(Ship.getNumberOfShips());
        for (Ship.Type type : Ship.Type.values()) {
            if (type != null) {
                mShips.add(new Ship(type));
            }
        }
    }

    /**
     * @return The total number of cells in this GameBoard.
     */
    public int getCellCount() {
        return getDimension() * getDimension();
    }

    /**
     * @return Dimension of a side of the GameBoard.
     */
    public int getDimension() {
        return mGameBoard.length;
    }

    /**
     * Returns the GameBoardCell at the given position.
     *
     * @param row
     * @param col
     * @return
     */
    public GameBoardCell getCell(int row, int col) {
        return mGameBoard[row][col];
    }

    /**
     * Attempts a shot at the specified board position
     *
     * @param row
     * @param col
     * @return The ship type if it is a hit, and null
     * if it is a miss.
     */
    public Ship.Type shootAt(int row, int col) {
        GameBoardCell cell = getCell(row, col);
        cell.setShot();

        mShots++;

        if (cell.getShipType() != null) {
            mHits++;
        }

        return cell.getShipType();
    }

    /**
     * @return the number of rows in the game board.
     */
    private int getRows() {
        return mGameBoard.length;
    }

    /**
     * @return the number of columns in the game board.
     */
    private int getColumns() {
        return mGameBoard[0].length;
    }

    /**
     * Determines if this ship can be placed (or moved) to the
     * specified starting location using the specified orientation
     * (or any orientation if Ship.Orientation.Any is specified).
     *
     * @param ship
     * @param row
     * @param col
     * @param orientation
     * @return the possible orientation(s) for this ship or null
     * if the ship can't be legally placed at the specified
     * position in any orientation.
     */
    public Ship.Orientation canPlaceShip(Ship ship,
                                         int row,
                                         int col,
                                         Ship.Orientation orientation) {

        if (orientation != Ship.Orientation.Horizontal &&
                orientation != Ship.Orientation.Vertical &&
                orientation != Ship.Orientation.Any) {
            throw new IllegalArgumentException(
                    "canPlaceShip requires an valid orientation or Any");
        }

        if (orientation == Ship.Orientation.Any) {
            // Recursively call this method to determine if
            // the ship can be placed horizontally, vertically,
            // or both.
            Ship.Orientation horizontal =
                    canPlaceShip(ship, row, col, Ship.Orientation.Horizontal);
            Ship.Orientation vertical =
                    canPlaceShip(ship, row, col, Ship.Orientation.Vertical);

            if (horizontal == Ship.Orientation.Horizontal &&
                    vertical == Ship.Orientation.Vertical) {
                return Ship.Orientation.Any;
            } else if (horizontal != null) {
                return horizontal;
            } else if (vertical != null) {
                return vertical;
            } else {
                return null;
            }
        }

        // At this point the requested orientation is either
        // horizontal or vertical (not any).
        switch (orientation) {
            case Horizontal: {
                if (col + ship.getLength() > getColumns()) {
                    // Illegal: shop exceeds board boundary.
                    return null;
                }

                for (int i = 0; i < ship.getLength(); i++) {
                    Ship.Type currentType = getCell(row, col + i).getShipType();
                    if (currentType != null && currentType != ship.getType()) {
                        // Illegal: ship intersects and existing ship.
                        return null;
                    }
                }

                return orientation;
            }

            case Vertical: {
                if (row + ship.getLength() > getRows()) {
                    // Illegal: shop exceeds board boundary.
                    return null;
                }

                for (int i = 0; i < ship.getLength(); i++) {
                    Ship.Type currentType = getCell(row + i, col).getShipType();
                    if (currentType != null && currentType != ship.getType()) {
                        // Illegal: ship intersects and existing ship.
                        return null;
                    }
                }

                return orientation;
            }

            default: {
                throw new IllegalStateException(
                        "canPlaceShip should have a " +
                                "horizontal or vertical orientation");
            }
        }
    }

    /**
     * Places the ship at the specified starting location
     * in the specified orientation. Since this method does
     * not check for illegal placement, canPlaceShip() must
     * first be called to avoid program faults.
     *
     * @param ship
     * @param row
     * @param col
     * @param orientation
     * @return
     */
    public boolean placeShip(Ship ship,
                             int row,
                             int col,
                             Ship.Orientation orientation) {

        if (orientation != Ship.Orientation.Horizontal &&
                orientation != Ship.Orientation.Vertical) {
            throw new IllegalArgumentException(
                    "placeShip requires a valid orientation");
        }

        Ship.Orientation orientationChoices =
                canPlaceShip(ship, row, col, orientation);

        if (orientationChoices == null) {
            return false;
        }

        if (ship.isPlaced()) {
            removeShip(ship);
        }

        ship.setOrientation(orientation);

        switch (orientation) {
            case Horizontal:
                for (int i = 0; i < ship.getLength(); i++) {
                    GameBoardCell cell = getCell(row, col + i);
                    cell.clear();
                    cell.setShipType(ship.getType());
                    ship.setCell(i, cell);
                }
                break;
            case Vertical:
                for (int i = 0; i < ship.getLength(); i++) {
                    GameBoardCell cell = getCell(row + i, col);
                    cell.clear();
                    cell.setShipType(ship.getType());
                    ship.setCell(i, cell);
                }
                break;
            case Any:
                throw new IllegalStateException(
                        "placeShip requires a valid orientation");
        }

        return true;
    }

    public static Ship.Orientation toggleOrientation(Ship.Orientation
                                                             orientation) {
        return orientation == Ship.Orientation.Horizontal ?
                Ship.Orientation.Vertical : Ship.Orientation.Horizontal;
    }

    /**
     * Removes the ship from the board.
     *
     * @param ship
     */
    public static void removeShip(@NonNull Ship ship) {
        if (ship.isPlaced()) {
            ship.clearCells();
        }
    }

    /**
     * Converts 2 dimensional array to 1 dimension for parcelizing.
     *
     * @param gameBoard
     * @return 1 dimensional array
     */
    private static GameBoardCell[] toOneDimension(GameBoardCell[][] gameBoard) {
        int rows = gameBoard.length;
        int cols = gameBoard[0].length;
        GameBoardCell[] output = new GameBoardCell[rows * cols];

        for (int i = 0; i < rows; i++) {
            System.arraycopy(gameBoard[i], 0, output, i * rows, cols);
        }

        return output;
    }

    /**
     * Converts 1 dimensional array to 2 dimensional array for unparcelizing.
     *
     * @param dimensions
     * @param gameBoard
     * @return 2 dimensional array
     */
    private static GameBoardCell[][] toTwoDimensions(
            int dimensions, GameBoardCell[] gameBoard) {

        GameBoardCell[][] output =
                new GameBoardCell[gameBoard.length / dimensions][dimensions];

        for (int i = 0; i < gameBoard.length; i++) {
            output[i / dimensions][i % dimensions] = gameBoard[i];
        }

        return output;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mHits);
        dest.writeInt(mShots);
        dest.writeString(mPlayer);

        dest.writeSerializable(this.mShips);
        dest.writeSerializable(this.mGameTurns);
        ArrayList<GameBoardCell> arrayList =
                new ArrayList<>(Arrays.asList(toOneDimension(mGameBoard)));

        dest.writeSerializable(arrayList);
    }

    private GameBoard(Parcel in) {
        mHits = in.readInt();
        mShots = in.readInt();
        mPlayer = in.readString();

        //noinspection unchecked
        mShips = (ArrayList<Ship>) in.readSerializable();
        //noinspection unchecked
        mGameTurns = (ArrayList<Integer>) in.readSerializable();
        @SuppressWarnings("unchecked")
        ArrayList<GameBoardCell> arrayList =
                (ArrayList<GameBoardCell>) in.readSerializable();

        GameBoardCell[] oneDimensionalBoard = arrayList.toArray(new
                GameBoardCell[arrayList.size()]);

        int dimension = (int) Math.sqrt(arrayList.size());
        mGameBoard = toTwoDimensions(dimension, oneDimensionalBoard);

        // Ships cells need to be reset to point to GameBoard cells.
        for (Ship ship : mShips) {
            for (int i = 0; i < ship.getLength(); i++) {
                int row = ship.getCell(i).getRow();
                int col = ship.getCell(i).getCol();
                GameBoardCell gameCell = getCell(row, col);
                ship.setCell(i, gameCell);
            }
        }
    }

    public static final Parcelable.Creator<GameBoard> CREATOR = new
            Parcelable.Creator<GameBoard>() {
                @Override
                public GameBoard createFromParcel(Parcel source) {
                    return new GameBoard(source);
                }

                @Override
                public GameBoard[] newArray(int size) {
                    return new GameBoard[size];
                }
            };

    /**
     * Generates a random placement of all ships.
     *
     * @return array of randomly placed ships.
     */
    public ArrayList<Ship> generateRandomPlacement() {
        // First clear current ships.
        reset();

        mShips = new ArrayList<>(Ship.getNumberOfShips());

        Random random = new Random();

        int numCells = getDimension() * getDimension();
        ArrayList<Integer> positions = new ArrayList<>(numCells);
        for (int i = 0; i < numCells; i++) {
            positions.add(i);
        }

        for (Ship.Type type : Ship.Type.values()) {
            Ship ship = new Ship(type);
            mShips.add(type.ordinal(), ship);

            do {
                int index = random.nextInt(positions.size());
                int id = positions.get(index);

                int row = id / getDimension();
                int col = id % getDimension();

                Ship.Orientation orientation =
                        canPlaceShip(ship, row, col, Ship.Orientation.Any);

                if (orientation == null) {
                    continue;
                }

                if (orientation == Ship.Orientation.Any) {
                    orientation = Ship.Orientation.values()[random.nextInt(2)];
                }

                if (placeShip(ship, row, col, orientation)) {
                    for (int i = 0; i < ship.getLength(); i++) {
                        GameBoardCell cell = ship.getCell(i);
                        id = cell.getRow() * getDimension() + cell.getCol();
                        positions.remove(Integer.valueOf(id));
                    }
                } else {
                    throw new IllegalStateException("Random placement failed!");
                }
            } while (!ship.isPlaced());
        }

        return mShips;
    }

    /**
     * Returns the ship of the given type from the list of ships.
     *
     * @param type
     * @return
     */
    public Ship getShip(Ship.Type type) {
        return type != null ? mShips.get(type.ordinal()) : null;
    }

    /**
     * Returns the ship of the given type from the list of ships.
     *
     * @param row
     * @param col
     * @return ship at the specified cell or null if cell is empty.
     */
    public Ship getShip(int row, int col) {
        GameBoardCell cell = getCell(row, col);
        return cell.containsShip() ? getShip(cell.getShipType()) : null;
    }

    /**
     * @return list of ships for this board.
     */
    public ArrayList<Ship> getShips() {
        return mShips;
    }

    /**
     * Resets the game board (clears all ship placements).
     */
    private void reset() {
        for (Ship ship : mShips) {
            removeShip(ship);
        }

        mHits = 0;
        mShots = 0;
    }

    /**
     * @return true if all ships sunk; false if not.
     */
    public boolean isLooser() {
        for (Ship ship : mShips) {
            if (!ship.isSunk()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Takes a random shot at a players game board.
     */
    public Ship.Type takeTurn() {
        if (mGameTurns.isEmpty()) {
            throw new IllegalStateException("No more cells to shoot at!");
        }
        Random random = new Random();
        int index = random.nextInt(mGameTurns.size());
        int id = mGameTurns.get(index);
        mGameTurns.remove(index);

        int row = id / getDimension();
        int col = id % getDimension();

        return shootAt(row, col);
    }

    public void newGame() {
        int numCells = getDimension() * getDimension();
        mGameTurns = new ArrayList<>(numCells);
        for (int i = 0; i < numCells; i++) {
            mGameTurns.add(i);
        }

        mHits = 0;
        mShots = 0;
    }

    private int getHits() {
        return mHits;
    }

    public int getShots() {
        return mShots;
    }

    public String getPlayer() {
        return mPlayer;
    }

    public void setPlayer(String player) {
        mPlayer = player;
    }
}
