package app.battleships.logic;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Ship implements Parcelable, Serializable {
    /**
     * Each GameBoard cell is either empty (Ship.Type null)
     * or contains a portion of one of the following ship
     * types (ascending order by size).
     */
    public enum Type {
        Destroyer,      // 2
        Cruiser,        // 3
        Submarine,      // 3
        Battleship,     // 4
        AircraftCarrier // 5
    }

    // Ships sizes.
    private static final int DestroyerLength = 2;
    private static final int CruiserLength = 3;
    private static final int SubmarineLength = 3;
    private static final int BattleshipLength = 4;
    private static final int AircraftCarrierLength = 5;

    private static final int[] mLengths = {
            DestroyerLength,
            CruiserLength,
            SubmarineLength,
            BattleshipLength,
            AircraftCarrierLength
    };

    // Used for attempts to place a ship.
    public enum Orientation {
        Horizontal,
        Vertical,
        Any
    }

    private final Type mType;

    private Orientation mOrientation;
    private GameBoardCell[] mCells;

    public Ship(Type type) {
        mType = type;
    }

    public Type getType() {
        return mType;
    }

    public int getRow() {
        return mCells != null ? mCells[0].getRow() : -1;
    }

    public int getCol() {
        return mCells != null ? mCells[0].getCol() : -1;
    }

    public int getLength() {
        return mLengths[getType().ordinal()];
    }

    public void setCell(int position, GameBoardCell cell) {
        if (mCells == null) {
            mCells = new GameBoardCell[getLength()];
        }

        mCells[position] = cell;
    }

    public boolean isPlaced() {
        return mCells != null;
    }

    public Orientation getOrientation() {
        return mOrientation;
    }

    public void setOrientation(Orientation orientation) {
        mOrientation = orientation;
    }

    public static int getNumberOfShips() {
        return Type.values().length;
    }

    public boolean isSunk() {
        if (mCells == null) {
            return false;
        }

        for (GameBoardCell cell : mCells) {
            if (!cell.isHit()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Clears ships game board cells.
     */
    public void clearCells() {
        for (GameBoardCell cell : mCells) {
            cell.clear();
        }

        mCells = null;
    }

    /**
     * @param index
     * @return the ship cell at the specified index or null.
     */
    public GameBoardCell getCell(int index) {
        if (mCells != null && 0 <= index && index < mCells.length) {
            return mCells[index];
        }

        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mType == null ? -1 : this.mType.ordinal());
        dest.writeInt(this.mOrientation == null ? -1 : this.mOrientation
                .ordinal());
        dest.writeTypedArray(this.mCells, flags);
    }

    private Ship(Parcel in) {
        int tmpMType = in.readInt();
        this.mType = tmpMType == -1 ? null : Type.values()[tmpMType];
        int tmpMOrientation = in.readInt();
        this.mOrientation = tmpMOrientation == -1 ? null : Orientation.values
                ()[tmpMOrientation];
        this.mCells = in.createTypedArray(GameBoardCell.CREATOR);
    }

    public static final Parcelable.Creator<Ship> CREATOR = new Parcelable
            .Creator<Ship>() {
        @Override
        public Ship createFromParcel(Parcel source) {
            return new Ship(source);
        }

        @Override
        public Ship[] newArray(int size) {
            return new Ship[size];
        }
    };
}
