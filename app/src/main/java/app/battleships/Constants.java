package app.battleships;

public class Constants {
    public static final int GAME_BOARD_SIZE = 9;
    public static final int SHIP_SELECTED_COLOR = 0xffff0000;
    public static final int SHIP_HIT_COLOR = 0xffff9900;
    public static final int SHIP_SUNK_COLOR = 0xffff0000;
    public static final int SHIP_COLOR = 0xff00ff00;
    public static final int WATER_COLOR = 0xff40a4df;
}
