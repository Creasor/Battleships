package app.battleships.adapters;

import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import app.battleships.Constants;
import app.battleships.R;
import app.battleships.logic.GameBoard;
import app.battleships.logic.GameBoardCell;
import app.battleships.logic.Ship;

public class GameBoardAdapter extends
        RecyclerView.Adapter<GameBoardAdapter.GameBoardViewHolder> {

    private final GameBoard mGameBoard;

    private OnGameBoardClickListener mOnGameBoardClickListener;

    private Ship mSelectedShip;
    private boolean mShowShips;

    /**
     * Constructor
     *
     * @param gameBoard
     */
    public GameBoardAdapter(GameBoard gameBoard) {
        mGameBoard = gameBoard;
    }

    /**
     * @return GameBoard data object for this adapter.
     */
    public GameBoard getGameBoard() {
        return mGameBoard;
    }

    /**
     * Shows or hides ships.
     *
     * @param show
     */
    public void setShowShips(boolean show) {
        mShowShips = show;
        notifyDataSetChanged();
    }

    @Override
    public GameBoardViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grid_cell, parent, false);

        return new GameBoardViewHolder(view);
    }

    /**
     * Returns the number of cells in the game board.
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return mGameBoard.getCellCount();
    }

    /**
     * Sets callback listener for cell clicks.
     *
     * @param listener
     */
    public void setGameBoardClickListener(OnGameBoardClickListener listener) {
        mOnGameBoardClickListener = listener;
    }

    /**
     * Maps a grid position to row and column to access
     * a GameBoardCell at that position.
     *
     * @param position
     */
    private GameBoardCell getCell(int position) {
        int row = position / mGameBoard.getDimension();
        int col = position % mGameBoard.getDimension();
        return mGameBoard.getCell(row, col);
    }

    @Override
    public void onBindViewHolder(GameBoardViewHolder holder, int position) {

        final int adapterPosition = holder.getAdapterPosition();

        GameBoardCell cell = getCell(adapterPosition);
        Ship ship = mGameBoard.getShip(cell.getShipType());

        String text = "";
        if (ship != null) {
            if (ship.isSunk()) {
                holder.mButton.getBackground().setColorFilter(
                        Constants.SHIP_SUNK_COLOR,
                        PorterDuff.Mode.MULTIPLY);
            } else if (cell.isHit()) {
                holder.mButton.getBackground().setColorFilter(
                        Constants.SHIP_HIT_COLOR,
                        PorterDuff.Mode.MULTIPLY);
            } else if (mShowShips) {
                if (mGameBoard.getShip(cell.getShipType()) == mSelectedShip) {
                    holder.mButton.getBackground().setColorFilter(
                            Constants.SHIP_SELECTED_COLOR,
                            PorterDuff.Mode.MULTIPLY);
                } else {
                    holder.mButton.getBackground().setColorFilter(
                            Constants.SHIP_COLOR,
                            PorterDuff.Mode.MULTIPLY);
                }
            } else {
                holder.mButton.getBackground().setColorFilter(
                        Constants.WATER_COLOR,
                        PorterDuff.Mode.MULTIPLY);
            }
        } else {
            holder.mButton.getBackground().setColorFilter(
                    Constants.WATER_COLOR,
                    PorterDuff.Mode.MULTIPLY);
            if (cell.isMiss()) {
                text = "~";
            }
        }

        holder.mButton.setText(text);

        // Set the id for possible use in automated testing.
        holder.mButton.setId(
                adapterPosition / mGameBoard.getDimension() +
                adapterPosition % mGameBoard.getDimension());

        holder.mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnGameBoardClickListener != null) {
                    mOnGameBoardClickListener.onCellClicked(
                            GameBoardAdapter.this,
                            adapterPosition / mGameBoard.getDimension(),
                            adapterPosition % mGameBoard.getDimension());
                }
            }
        });
    }

    public void setSelectedShip(Ship ship) {
        mSelectedShip = ship;
        notifyDataSetChanged();
    }

    public static class GameBoardViewHolder extends RecyclerView.ViewHolder {
        final Button mButton;

        public GameBoardViewHolder(View itemView) {
            super(itemView);
            mButton = (Button) itemView.findViewById(R.id.grid_cell_button);
        }
    }

    public interface OnGameBoardClickListener {
        void onCellClicked(GameBoardAdapter adapter, int row, int col);
    }
}
