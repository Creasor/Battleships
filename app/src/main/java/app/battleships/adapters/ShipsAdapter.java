package app.battleships.adapters;

import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import app.battleships.Constants;
import app.battleships.R;
import app.battleships.logic.Ship;

public class ShipsAdapter extends
        RecyclerView.Adapter<ShipsAdapter.ShipViewHolder> {

    private ArrayList<Ship> mShips;

    private OnShipClickListener mOnShipClickListener;

    private int mSelectedItem = -1;

    @Override
    public ShipViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ship, parent, false);

        // View type maps to ship index;
        Ship ship = mShips.get(viewType);
        return new ShipViewHolder(view, ship);
    }

    /**
     * Sets the initial list of unplaced ships to display.
     *
     * @param ships
     */
    public void setData(ArrayList<Ship> ships) {
        mShips = ships;
        mSelectedItem = -1;
        notifyDataSetChanged();
    }

    /**
     * Selected the specified item.
     *
     * @param item
     */
    public void setSelectedItem(int item) {
        mSelectedItem = item;
        notifyDataSetChanged();
    }

    /**
     * Selected the specified item.
     *
     * @param ship
     */
    public void setSelectedItem(Ship ship) {
        mSelectedItem = ship != null ? mShips.indexOf(ship) : -1;
        notifyDataSetChanged();
    }

    /**
     * @return The currently selected ship.
     */
    public Ship getSelectedShip() {
        return mSelectedItem != -1 ? mShips.get(mSelectedItem) : null;
    }

    /**
     * Returns the number of cells in the game board.
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return mShips.size();
    }

    /**
     * Since the each item in the list is dynamically
     * created and uses a different template, we have
     * to return a unique view type for each list item
     * so the RecyclerView does not try to reuse view
     * holders. This is not an issue because the list
     * is very small.
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Sets callback listener for cell clicks.
     *
     * @param listener
     */
    public void setShipClickListener(OnShipClickListener listener) {
        mOnShipClickListener = listener;
    }

    @Override
    public void onBindViewHolder(ShipViewHolder holder, int position) {

        final int adapterPosition = holder.getAdapterPosition();
        Ship ship = mShips.get(adapterPosition);

        for (int i = 0; i < ship.getLength(); i++) {
            for (int j = 0; j < ship.getLength(); j++) {
                if (adapterPosition == mSelectedItem) {
                    holder.mButtons[i].getBackground().setColorFilter(
                            Constants.SHIP_SELECTED_COLOR,
                            PorterDuff.Mode.MULTIPLY);
                } else if (ship.isPlaced()) {
                    holder.mButtons[i].getBackground().setColorFilter(
                            Constants.SHIP_COLOR,
                            PorterDuff.Mode.MULTIPLY);
                } else {
                    holder.mButtons[i].getBackground().setColorFilter(null);
                }
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnShipClickListener != null) {
                    Ship ship = mShips.get(adapterPosition);
                    mOnShipClickListener.onShipClicked(ship);
                }
            }
        });
    }

    /**
     * Returns the specified ship.
     *
     * @param position
     * @return
     */
    public Ship getItem(int position) {
        return mShips.get(position);
    }

    public static class ShipViewHolder extends RecyclerView.ViewHolder {
        Button mButtons[];

        public ShipViewHolder(View view, Ship ship) {
            super(view);
            buildShipLayout((ViewGroup) view, ship);
        }

        private void buildShipLayout(ViewGroup parent, Ship ship) {
            int shipLength = ship.getLength();
            mButtons = new Button[shipLength];

            for (int i = 0; i < shipLength; i++) {
                // Use LayoutInflater to create a parent-less view from XML.
                @SuppressWarnings("RedundantCast")
                View view = LayoutInflater.from(itemView.getContext())
                        .inflate(R.layout.item_grid_cell, (ViewGroup)null);

                view.setId(i);
                view.setClickable(false);
                int width = (int)parent.getContext().getResources()
                        .getDimension(R.dimen.ship_width);
                int height = (int)parent.getContext().getResources()
                        .getDimension(R.dimen.ship_height);
                ViewGroup.LayoutParams layoutParams =
                        new ViewGroup.LayoutParams(width, height);
                view.setLayoutParams(layoutParams);
                mButtons[i] = (Button)view;
                parent.addView(view);
            }
        }
    }

    public interface OnShipClickListener {
        void onShipClicked(Ship ship);
    }
}
