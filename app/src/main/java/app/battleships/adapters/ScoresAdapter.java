package app.battleships.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import app.battleships.R;
import app.battleships.activities.ScoreActivity;

public class ScoresAdapter extends
        RecyclerView.Adapter<ScoresAdapter.ScoresViewHolder> {

    private ArrayList<ScoreActivity.Score> mScores;

    @Override
    public ScoresAdapter.ScoresViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_score, parent, false);

        return new ScoresViewHolder(view);
    }

    /**
     * Sets the list of scores.
     *
     * @param scores
     */
    public void setData(ArrayList<ScoreActivity.Score> scores) {
        mScores = scores;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mScores.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ScoresViewHolder holder, int position) {
        final int adapterPosition = holder.getAdapterPosition();
        ScoreActivity.Score score = mScores.get(adapterPosition);
        holder.mPlayer1.setText(score.mPlayer1);
        holder.mPlayer2.setText(score.mPlayer2);
        holder.mShots.setText(String.valueOf(score.mShots));
        holder.mPoints.setText(String.valueOf(score.mPoints));
    }

    public static class ScoresViewHolder extends RecyclerView.ViewHolder {
        final TextView mPlayer1;
        final TextView mPlayer2;
        final TextView mShots;
        final TextView mPoints;

        public ScoresViewHolder(View view) {
            super(view);
            mPlayer1 = (TextView)view.findViewById(R.id.player1_text_view);
            mPlayer2 = (TextView)view.findViewById(R.id.player2_text_view);
            mShots = (TextView)view.findViewById(R.id.shots_text_view);
            mPoints = (TextView)view.findViewById(R.id.points_text_view);
        }
    }
}
