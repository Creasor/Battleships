package app.battleships.utils;

import android.content.Context;
import android.widget.Toast;

public class Toaster {
    private static Toast toast;

    public static void show(Context context, String text) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
