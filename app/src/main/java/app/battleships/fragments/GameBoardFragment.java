package app.battleships.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.battleships.R;
import app.battleships.logic.GameBoard;
import app.battleships.adapters.GameBoardAdapter;
import app.battleships.logic.Ship;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the
 * {@link OnGameBoardFragmentListener}
 * interface.
 */
public class GameBoardFragment extends Fragment
        implements GameBoardAdapter.OnGameBoardClickListener {

    // Private keys used for passing bundle arguments to fragment.
    private static final String GAME_BOARD_KEY = "GameBoard";
    private static final String GAME_BOARD_SHOW_SHIPS = "GameBoardShowShips";

    private GameBoard mGameBoard;

    private GameBoardAdapter mGameBoardAdapter;


    private OnGameBoardFragmentListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GameBoardFragment() {
    }

    @SuppressWarnings("unused")
    public static GameBoardFragment newInstance(GameBoard gameBoard, boolean showShips) {
        GameBoardFragment fragment = new GameBoardFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(GAME_BOARD_KEY, gameBoard);
        bundle.putBoolean(GAME_BOARD_SHOW_SHIPS, showShips);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_gameboard, container, false);

        Bundle bundle = getArguments();
        mGameBoard = bundle.getParcelable(GAME_BOARD_KEY);
        boolean mShowShips = bundle.getBoolean(GAME_BOARD_SHOW_SHIPS);

        RecyclerView mRecyclerView = (RecyclerView)
                view.findViewById(R.id.game_board_recycler_view);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager
                (getContext(), mGameBoard.getDimension());
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mGameBoardAdapter = new GameBoardAdapter(mGameBoard);
        mGameBoardAdapter.setGameBoardClickListener(this);
        mRecyclerView.setAdapter(mGameBoardAdapter);

        mGameBoardAdapter.setShowShips(mShowShips);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnGameBoardFragmentListener) {
            mListener = (OnGameBoardFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGameBoardFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public GameBoard getGameBoard() {
        return mGameBoard;
    }

    @Override
    public void onCellClicked(GameBoardAdapter adapter, int row, int col) {
        mListener.onCellClicked(adapter, row, col);
    }

    public boolean placeShip(Ship ship, int row, int col) {

        if (ship.isPlaced()) {
            // Ship is placed. Check if it toggle or remove operation.
            if (ship.getRow() == row && ship.getCol() == col) {
                // Clicking on the first cell of a placed ship
                // toggles its orientation or removes the ship.
                Ship.Orientation orientation =
                        mGameBoard.canPlaceShip(
                                ship, row, col, Ship.Orientation.Any);
                if (orientation != Ship.Orientation.Any) {
                    // There is only one possible orientation so
                    // clicking removes the ship.
                    GameBoard.removeShip(ship);
                    return true;
                }

                // Both orientations are possible, remove the ship if
                // the current orientation is vertical.
                if (ship.getOrientation() == Ship.Orientation.Vertical) {
                    GameBoard.removeShip(ship);
                    return true;
                }

                // Get toggled orientation.
                orientation = GameBoard.toggleOrientation(ship.getOrientation());

                // We already know that this orientation is possible so
                // just do the placement.
                mGameBoard.placeShip(ship, row, col, orientation);
                mGameBoardAdapter.notifyDataSetChanged();
                return true;
            }
        }

        // Clicked on some cell other than the starting cell of the ship.
        // Determine the possible orientation choices for placing the
        // ship at this location.
        Ship.Orientation orientation =
                mGameBoard.canPlaceShip(ship, row, col, Ship.Orientation.Any);
        if (orientation == null) {
            return false;
        }

        switch (orientation) {
            case Horizontal:
            case Vertical: {
                boolean status =
                        mGameBoard.placeShip(ship, row, col, orientation);
                if (status) {
                    mGameBoardAdapter.notifyDataSetChanged();
                }
                return status;
            }

            case Any: {
                // Both orientations are legal so start with horizontal.
                boolean status =
                        mGameBoard.placeShip(
                                ship, row, col, Ship.Orientation.Horizontal);
                if (status) {
                    mGameBoardAdapter.notifyDataSetChanged();
                }
                return status;
            }
            default:
                return false;
        }
    }

    /**
     * Updates the adapter and display.
     */
    public void notifyChanged() {
        mGameBoardAdapter.notifyDataSetChanged();
    }


    public interface OnGameBoardFragmentListener {
        void onCellClicked(GameBoardAdapter adapter, int row, int col);
    }

    /**
     * Sets the currently selected ship (for setup mode placement).
     * @param ship
     */
    public void setSelectedShip(Ship ship) {
        mGameBoardAdapter.setSelectedShip(ship);
    }
}
