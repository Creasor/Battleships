package app.battleships.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import app.battleships.R;
import app.battleships.adapters.ShipsAdapter;
import app.battleships.logic.GameBoard;
import app.battleships.logic.Ship;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the
 * {@link OnShipsFragmentListener}
 * interface.
 */
public class ShipsFragment extends Fragment
        implements ShipsAdapter.OnShipClickListener {

    private static final String ARG_SHIPS_LIST = "ships_list";
    private ShipsAdapter mShipsAdapter;

    private OnShipsFragmentListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShipsFragment() {
    }

    @SuppressWarnings("unused")
    public static ShipsFragment newInstance(ArrayList<Ship> ships) {
        ShipsFragment fragment = new ShipsFragment();
        Bundle bundle = new Bundle();

        bundle.putSerializable(ARG_SHIPS_LIST, ships);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(
                R.layout.fragment_ships, container, false);

        Bundle bundle = getArguments();

        @SuppressWarnings("unchecked")
        ArrayList<Ship> ships =
                (ArrayList<Ship>) bundle.getSerializable(ARG_SHIPS_LIST);

        assert ships != null;

        RecyclerView mRecyclerView = (RecyclerView)
                view.findViewById(R.id.ships_recycler_view);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(
                getContext(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mShipsAdapter = new ShipsAdapter();
        mShipsAdapter.setShipClickListener(this);
        mRecyclerView.setAdapter(mShipsAdapter);
        mShipsAdapter.setData(ships);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShipsFragmentListener) {
            mListener = (OnShipsFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShipsFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onShipClicked(Ship ship) {
        if (mListener != null) {
            mListener.onShipClicked(ship);
        }
    }

    public void setSelectedShip(Ship ship) {
        mShipsAdapter.setSelectedItem(ship);
    }

    /**
     * @return returns true if all ships have been placed; false if not.
     */
    public boolean areAllShipsPlaced() {
        int count = 0;
        for (int i = 0; i < mShipsAdapter.getItemCount(); i++) {
            Ship ship = mShipsAdapter.getItem(i);
            if (ship.isPlaced()) {
                count++;
            }
        }

        return count == mShipsAdapter.getItemCount();
    }

    /**
     * Clears all ships positions on the board.
     */
    public void resetShips() {
        for (int i = 0; i < mShipsAdapter.getItemCount(); i++) {
            Ship ship = mShipsAdapter.getItem(i);
            GameBoard.removeShip(ship);
        }
        mShipsAdapter.setSelectedItem(-1);
        mShipsAdapter.notifyDataSetChanged();
    }

    /**
     * Resets all ships to the passed list of ships.
     *
     * @param ships
     */
    public void setShips(ArrayList<Ship> ships) {
        mShipsAdapter.setData(ships);
    }


    /**
     * @return selected ship (from adapter).
     */
    public Ship getSelectedShip() {
        return mShipsAdapter.getSelectedShip();
    }

    public interface OnShipsFragmentListener {
        void onShipClicked(Ship ship);
    }
}
