package app.battleships.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import app.battleships.Constants;
import app.battleships.R;
import app.battleships.utils.Toaster;
import app.battleships.fragments.GameBoardFragment;
import app.battleships.fragments.ShipsFragment;
import app.battleships.logic.GameBoard;
import app.battleships.adapters.GameBoardAdapter;
import app.battleships.logic.Ship;

public class SetupActivity extends AppCompatActivity implements
        GameBoardFragment.OnGameBoardFragmentListener,
        ShipsFragment.OnShipsFragmentListener {

    private GameBoardFragment mGameBoardFragment;
    private ShipsFragment mShipsFragment;
    private Button mStartButton;
    private GameBoard mGameBoard;
    private TextView mPlayerTextView;
    private String mPlayerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        //noinspection ConstantConditions
        getSupportActionBar().setTitle(R.string.title_activity_setup);

        mGameBoard = new GameBoard(Constants.GAME_BOARD_SIZE);
        mGameBoardFragment = GameBoardFragment.newInstance(mGameBoard, true);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.game_board_content, mGameBoardFragment)
                .commit();

        mShipsFragment = ShipsFragment.newInstance(mGameBoard.getShips());

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.ships_content, mShipsFragment)
                .commit();

        // Initialize all view members.
        initializeViews();

        // Restore saved session preferences.
        restoreSessionPreferences();

        // Show user name entry dialog.
        showUserNameDialog();
    }

    private void initializeViews() {
        mStartButton = (Button) findViewById(R.id.start_button);
        assert mStartButton != null;
        mStartButton.setEnabled(false);
        mPlayerTextView = (TextView)findViewById(R.id.player_text_view);
    }

    /**
     * Displays a user name entry dialog when activity first starts.
     */
    private void showUserNameDialog() {
        final AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(this);

        final EditText editText = new EditText(this);
        editText.setSingleLine();
        if (mPlayerName != null && !mPlayerName.isEmpty()) {
            editText.setText(mPlayerName);
        }

        editText.setHint("Enter player name");

        // Set dialog message.
        alertDialogBuilder
                .setTitle("Player Login")
                .setMessage("Enter you name")
                .setCancelable(false)
                .setView(editText)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                mPlayerName = editText.getText().toString();
                                if (mPlayerName.isEmpty()) {
                                    mPlayerName = "Anonymous";
                                }
                                mPlayerTextView.setText(mPlayerName);
                                mGameBoard.setPlayer(mPlayerName);
                            }
                        });

        alertDialogBuilder.create().show();
    }

    /**
     * Override to save session preferences.
     */
    @Override
    protected void onStop() {
        super.onStop();
        saveSessionPreferences();
    }

    /**
     * XML callback for resetting game board (removes all ships).
     *
     * @param v
     */
    public void onClickRandom(View v) {
        mShipsFragment.setShips(mGameBoard.generateRandomPlacement());
        mGameBoardFragment.notifyChanged();
        mStartButton.setEnabled(isBoardSetup());
    }

    /**
     * XML callback for resetting game board (removes all ships).
     *
     * @param v
     */
    public void onClickReset(View v) {
        mShipsFragment.resetShips();
        mGameBoardFragment.setSelectedShip(null);
        mGameBoardFragment.notifyChanged();
        mStartButton.setEnabled(isBoardSetup());
    }

    /**
     * XML callback for start game button.
     *
     * @param v
     */
    public void onClickStartGame(View v) {
        // Clear selected ship.
        mGameBoardFragment.setSelectedShip(null);
        mShipsFragment.setSelectedShip(null);
        GameBoard gameBoard = mGameBoardFragment.getGameBoard();
        startActivity(GameActivity.makeIntent(this, gameBoard));
    }

    /**
     * Handles ship clicks from ShipsFragment
     *
     * @param ship
     */
    @Override
    public void onShipClicked(Ship ship) {
        selectShip(ship);
    }

    /**
     * Handles GameBoard cell clicks from GameBoardFragment
     *
     * @param adapter
     * @param row
     * @param col
     */
    @Override
    public void onCellClicked(GameBoardAdapter adapter, int row, int col) {
        GameBoard gameBoard = mGameBoardFragment.getGameBoard();
        Ship ship = gameBoard.getShip(row, col);

        // If no ship is currently selected and the user clicks on
        // an empty cell, then warn them that the need to first
        // select a ship in order to place it on the board.
        if (getSelectedShip() == null) {
            if (ship == null) {
                Toaster.show(this, "You must first select a ship");
            } else {
                // Since we have no selected ship and the user
                // has clicked on a ship, automatically select
                // it in the ship list.
                selectShip(ship);
            }
            return;
        } else if (ship != null) {
            // We already have a selected ship and the user has clicked
            // on some kind of ship...
            if (ship != getSelectedShip()) {
                // The user clicked on a different ship so just
                // change the selection and return.
                selectShip(ship);
                return;
            }
        }

        // Now let the ship fragment handle placing, orienting,
        // or deselecting this ship.
        boolean placed =
                mGameBoardFragment.placeShip(getSelectedShip(), row, col);

        adapter.notifyDataSetChanged();
        if (!placed) {
            Toaster.show(this, "Illegal position. Try again.");
        }

        // Always update the start button enabled state after
        // each click based on if all ships are currently placed
        // in legal positions on the board.
        mStartButton.setEnabled(isBoardSetup());
    }

    /**
     * Sets the currently selected ship in both fragments.
     *
     * @param ship
     */
    private void selectShip(Ship ship) {
        mGameBoardFragment.setSelectedShip(ship);
        mShipsFragment.setSelectedShip(ship);
    }

    /**
     * @return returns true if all ships are on the board; false if not.
     */
    private boolean isBoardSetup() {
        return mShipsFragment.areAllShipsPlaced();
    }

    private Ship getSelectedShip() {
        return mShipsFragment.getSelectedShip();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveSessionPreferences();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreSessionPreferences();
    }

    /**
     * Save session preferences.
     */
    private void saveSessionPreferences() {
        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("playerName", mPlayerName);
        editor.apply();
    }

    /**
     * Restore session preferences.
     */
    private void restoreSessionPreferences() {
        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(this);
        mPlayerName = prefs.getString("playerName", "");
        mPlayerTextView.setText(mPlayerName);
    }
}
