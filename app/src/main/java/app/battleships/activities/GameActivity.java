package app.battleships.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import app.battleships.Constants;
import app.battleships.R;
import app.battleships.utils.Toaster;
import app.battleships.fragments.GameBoardFragment;
import app.battleships.logic.GameBoard;
import app.battleships.adapters.GameBoardAdapter;
import app.battleships.logic.GameBoardCell;

public class GameActivity extends AppCompatActivity
        implements GameBoardFragment.OnGameBoardFragmentListener {
    // Key used to store passed GameBoard as an intent extra.
    private static final String INTENT_EXTRA_GAMEBOARD = "gameboard";

    private GameBoardFragment mPlayerGameBoardFragment;
    private GameBoardFragment mAndroidGameBoardFragment;
    private GameBoard mPlayerGameBoard;
    private GameBoard mAndroidGameBoard;

    /**
     * Creates an intent that can be used to start this activity.
     *
     * @param gameBoard
     * @return
     */
    public static Intent makeIntent(
            Context context, GameBoard gameBoard) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(INTENT_EXTRA_GAMEBOARD, (Parcelable) gameBoard);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_game);

        //noinspection ConstantConditions
        getSupportActionBar().setTitle(R.string.title_activity_game);

        Intent intent = getIntent();

        mPlayerGameBoard = intent.getParcelableExtra(INTENT_EXTRA_GAMEBOARD);
        mPlayerGameBoard.newGame();

        TextView playerTextView = (TextView)findViewById(R.id.player_text_view);
        assert playerTextView != null;
        playerTextView.setText(mPlayerGameBoard.getPlayer());

        mPlayerGameBoardFragment =
                GameBoardFragment.newInstance(mPlayerGameBoard, true);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.player_content, mPlayerGameBoardFragment)
                .commit();

        mAndroidGameBoard = new GameBoard(Constants.GAME_BOARD_SIZE);
        mAndroidGameBoard.generateRandomPlacement();
        mAndroidGameBoard.newGame();
        mAndroidGameBoard.setPlayer("Android");
        TextView androidTextView = (TextView)findViewById(R.id.android_text_view);

        assert androidTextView != null;
        androidTextView.setText(mAndroidGameBoard.getPlayer());

        mAndroidGameBoardFragment =
                GameBoardFragment.newInstance(mAndroidGameBoard, false);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.android_content, mAndroidGameBoardFragment)
                .commit();
    }

    @Override
    public void onCellClicked(GameBoardAdapter adapter, int row, int col) {

        if (mAndroidGameBoard.isLooser() || mPlayerGameBoard.isLooser()) {
            Toaster.show(this, "This game is already over");
            return;
        }

        if (adapter.getGameBoard() == mPlayerGameBoardFragment.getGameBoard()) {
            Toaster.show(this, "You can't shoot at yourself!");
            return;
        }

        GameBoardCell cell = mAndroidGameBoard.getCell(row, col);

        boolean gameWon = false;
        ScoreActivity.Score score = null;

        if (cell.isHit() || cell.isMiss()) {
            Toaster.show(this, "You have already shot at this location");
            if (mAndroidGameBoard.isLooser()) {
                Toaster.show(this, "You have already won!");
            }
        } else {
            if (mAndroidGameBoard.shootAt(row, col) != null) {
                Toaster.show(this, "Nice shot!");
            }
            mAndroidGameBoardFragment.notifyChanged();
            if (mAndroidGameBoard.isLooser()) {
                Toaster.show(GameActivity.this, "You have won!");
                gameWon = true;
                score = new ScoreActivity.Score(
                        mPlayerGameBoard.getPlayer(),
                        mAndroidGameBoard.getPlayer(),
                        mAndroidGameBoard.getShots(),
                        100 - mAndroidGameBoard.getShots());
            }

            if (mPlayerGameBoard.takeTurn() != null) {
                if (mPlayerGameBoard.isLooser()) {
                    Toaster.show(this, "You have lost!");
                    gameWon = true;
                    score = new ScoreActivity.Score(
                            mAndroidGameBoard.getPlayer(),
                            mPlayerGameBoard.getPlayer(),
                            mPlayerGameBoard.getShots(),
                            100 - mPlayerGameBoard.getShots());
                } else {
                    Toaster.show(this, "You've been hit!");
                }
            }

            mPlayerGameBoardFragment.notifyChanged();
        }

        if (gameWon) {
            startActivity(ScoreActivity.makeIntent(this, score));
        }
    }
}
