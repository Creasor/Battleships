package app.battleships.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import app.battleships.R;
import app.battleships.adapters.ScoresAdapter;

public class ScoreActivity extends AppCompatActivity {

    private static final String INTEXT_EXTRA_SCORES = "scores";

    private ArrayList<Score> mScores;

    public static Intent makeIntent(Context context, Score score) {
        Intent intent = new Intent(context, ScoreActivity.class);
        intent.putExtra(INTEXT_EXTRA_SCORES, score);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        //noinspection ConstantConditions
        getSupportActionBar().setTitle(R.string.title_activity_score);

        Intent intent = getIntent();
        Score score = (Score)intent.getSerializableExtra(INTEXT_EXTRA_SCORES);

        updateScores(score);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id
                .score_recycler_view);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        ScoresAdapter mAdapter = new ScoresAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setData(mScores);

    }

    private void updateScores(Score score) {
        mScores = restoreSessionPreferences(this);

        if (mScores.size() == 0) {
            buildDummyScores();
        }

        if (mScores.size() < 5) {
            mScores.add(score);
        } else {
            int item = -1;
            int lowestPoints = Integer.MAX_VALUE;
            for (int i = 0; i < mScores.size(); i++) {
                if (mScores.get(i).mPoints < lowestPoints) {
                    item = i;
                }
            }

            if (score.mPoints > lowestPoints) {
                mScores.remove(item);
                mScores.add(score);
            }
        }

        Collections.sort(mScores);

        saveSessionPreferences(this, mScores);
    }

    private void buildDummyScores() {
        mScores = new ArrayList<>(5);
        mScores.add(new Score("Mike", "George", 50, 100 - 50));
        mScores.add(new Score("Chown", "Android", 60, 100 - 60));
        mScores.add(new Score("Frank", "Android", 57, 100 - 57));
        mScores.add(new Score("Android", "Paul", 70, 100 - 70));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveSessionPreferences(this, mScores);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreSessionPreferences(this);
    }

    /**
     * Save session preferences.
     */
    private static void saveSessionPreferences(
            Context context, ArrayList<Score> scores) {
        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt("scores", scores.size());

        for (int i = 0; i < scores.size(); i++) {
            editor.putString(String.valueOf(i), scores.get(i).toString());
        }

        editor.apply();
    }

    /**
     * Restore session preferences.
     */
    private static ArrayList<Score> restoreSessionPreferences(Context context) {
        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(context);
        int count = prefs.getInt("scores", 0);
        ArrayList<Score> scores = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String string = prefs.getString(String.valueOf(i), null);
            if (string != null) {
                scores.add(new Score(string));
            }
        }

        return scores;
    }

    public static class Score implements Serializable, Comparable<Score> {
        public String mPlayer1;
        public String mPlayer2;
        public int mShots;
        public int mPoints;

        public Score(String p1, String p2, int shots, int points) {
            mPlayer1 = p1;
            mPlayer2 = p2;
            mShots = shots;
            mPoints = points;
        }

        public Score(String string) {
            fromString(string);
        }

        @Override
        public String toString() {
            return mPlayer1 + "," + mPlayer2 + "," + String.valueOf(mShots) + "," + String.valueOf(mPoints);
        }

        public void fromString(String string) {
            final String[] split = string.split(",");
            mPlayer1 = split[0];
            mPlayer2 = split[1];
            mShots = Integer.valueOf(split[2]);
            mPoints = Integer.valueOf(split[3]);
        }

        @Override
        public int compareTo(@NonNull Score another) {
            if (mPoints < another.mPoints) {
                return 1;
            } else if (mPoints > another.mPoints) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
